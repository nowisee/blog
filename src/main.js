// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import ElementUI from 'element-ui'
import axios from 'axios';
import 'element-ui/lib/theme-chalk/index.css';
import store from './vuex/store';
import "@/style/common.scss";
import {serveUrl} from '@/common/config.js'

Vue.use(ElementUI);
Vue.prototype.$axios = axios;
Vue.config.productionTip = false;


axios.defaults.baseURL = serveUrl;


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
