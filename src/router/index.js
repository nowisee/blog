import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('@/pages/home/index.vue')
    },
    {
      path: '/create',
      name: 'create',
      component: () => import('@/pages/create/index.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('@/pages/login/index.vue')
    },
    {
      path: '/articlePage',
      name: 'articlePage',
      component: () => import('@/pages/articlePage/index.vue')
    },
    {
      path: '/user',
      name: 'user',
      component: () => import('@/pages/user/index.vue'),
    },
    {
      path: '/setting',
      name: 'setting',
      component: () => import('@/pages/user/setting.vue')
    }
  ]
})
