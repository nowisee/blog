import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);


const state = JSON.parse(localStorage.getItem('state')) || { state: { userinfo: null } };

const mutations = {
    setUserinfo(state, userinfo) {
        console.log('setUserinfo');
        state.userinfo = userinfo;
    }
}
export default new Vuex.Store({
    state,
    mutations,
})